package ua.cn.stu.rental.integration;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import static org.junit.Assert.assertNotNull;

/**
 *
 */
@ContextConfiguration(locations = "file:src/main/webapp/WEB-INF/persistence-ua.cn.stu.rental.ws.beans.xml")
public class HibernateConfigurationTest extends AbstractJUnit4SpringContextTests {
    private final Logger LOG = LogManager.getLogger(HibernateConfigurationTest.class);
    @Autowired
    private SessionFactory sessionFactory;

    @Test
    public void testHibernateConfiguration() {
        LOG.debug("Starting testHibernateConfiguration");
        assertNotNull(sessionFactory);
    }
}
