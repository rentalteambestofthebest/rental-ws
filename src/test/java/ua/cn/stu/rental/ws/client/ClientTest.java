package ua.cn.stu.rental.ws.client;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.ws.client.core.WebServiceTemplate;
import ua.cn.stu.rental.ws.beans.Advertisement;
import ua.cn.stu.rental.ws.beans.CreateAdvertisementRequest;
import ua.cn.stu.rental.ws.beans.CreateAdvertisementResponse;
import static org.junit.Assert.*;

@ContextConfiguration(locations = "file:src/main/webapp/WEB-INF/persistence-ua.cn.stu.rental.ws.beans.xml")
public class ClientTest extends AbstractJUnit4SpringContextTests {
    @Autowired
    private WebServiceTemplate webServiceTemplate;

    @Test
    public void testClient() {
        CreateAdvertisementRequest request = new CreateAdvertisementRequest();
        Advertisement adv = new Advertisement();
        adv.setAddress("ad");
        request.setAd(adv);

        assertNotNull(webServiceTemplate);
        CreateAdvertisementResponse response =
                (CreateAdvertisementResponse)webServiceTemplate.marshalSendAndReceive(request);
        assertNotNull(response);
        assertNotNull(response.getAd());
        assertNotNull(response.getAd().getAddress());
        System.out.println(response.getId());
        System.out.println(response.getAd().getAddress());
    }
}
