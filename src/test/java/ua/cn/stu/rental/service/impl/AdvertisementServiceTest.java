package ua.cn.stu.rental.service.impl;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.rental.dao.IAdvertisementDAO;
import ua.cn.stu.rental.dao.hibernate.AdvertisementDAO;
import ua.cn.stu.rental.domain.Advertisement;
import ua.cn.stu.rental.services.IAdvertisementService;
import ua.cn.stu.rental.services.impl.AdvertisementService;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class AdvertisementServiceTest extends BasicTest {

    private static IAdvertisementService service;

    private static IAdvertisementDAO mockDAO;

    private Advertisement advertisement;


    @Before
    public void setUp() throws Exception {
        advertisement = getAdvertisement();
    }

    @BeforeClass
    public static void init() {
        mockDAO = mock(AdvertisementDAO.class);
        service = new AdvertisementService(mockDAO);
    }

    @AfterClass
    public static void free() {
        mockDAO = null;
        service = null;
    }

    @Test
    public void testAdd() throws Exception {

        when(mockDAO.add(any(Advertisement.class))).thenReturn(1);
        advertisement.setId(1);
        when(mockDAO.find(any(Integer.class))).thenReturn(advertisement);

        Advertisement test = getAdvertisement();
        Advertisement add = service.add(test);
        assertNotNull(add);
        assertEquals(advertisement.getId(), add.getId());
    }

    @Test
    public void testUpdate() throws Exception {
        Advertisement edit = getAdvertisement();
        edit.setTitle("editTitle");
        when(mockDAO.edit(any(Advertisement.class))).thenReturn(edit);

        Advertisement testEdit = service.edit(advertisement);
        assertNotNull(testEdit);
        assertEquals("editTitle", testEdit.getTitle());
    }

    @Test
    public void testDelete() throws Exception {
        doNothing().when(mockDAO).delete(any(Advertisement.class));
        when(mockDAO.find(1)).thenReturn(null);

        service.delete(advertisement);
        Advertisement find = mockDAO.find(1);
        assertNull(find);
    }

    @Test
    public void testCreateAd() throws Exception {
        when(mockDAO.add(any(Advertisement.class))).thenReturn(1);
        advertisement.setId(1);
        when(mockDAO.find(any(Integer.class))).thenReturn(advertisement);

        Advertisement test = getAdvertisement();
        Advertisement add = service.add(test);
        assertNotNull(add);
        assertEquals(advertisement.getId(), add.getId());
    }

    @Test
    public void testGetAdsByUser() throws Exception {
        List<Advertisement> ads = new LinkedList<>();
        ads.add(getAdvertisement());
        ads.add(getAdvertisement());
        when(mockDAO.getAbByUser(anyMap(), anyMap(), anyInt(), anyInt())).thenReturn(ads);

        List<Advertisement> serviceAds = service.getAdsByUser(new HashMap<String, Object>(), new HashMap<String, List<Object>>(), 0, 10);

        assertNotNull(serviceAds);
        assertEquals(2, serviceAds.size());
    }

    @Test
    public void testGetFilteredAds() throws Exception {
        List<Advertisement> ads = new LinkedList<>();
        ads.add(getAdvertisement());
        ads.add(getAdvertisement());
        when(mockDAO.getFilteredAd(anyMap(), anyMap(), anyInt(), anyInt())).thenReturn(ads);

        List<Advertisement> serviceAds = service.getFilteredAds(new HashMap<String, Object>(), new HashMap<String, List<Object>>(), 0, 10);

        assertNotNull(serviceAds);
        assertEquals(2, serviceAds.size());
    }
}
