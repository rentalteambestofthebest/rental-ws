package ua.cn.stu.rental.service.impl;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.cn.stu.rental.dao.IRegisteredUserDAO;
import ua.cn.stu.rental.dao.IUserRoleDAO;
import ua.cn.stu.rental.dao.hibernate.RegisteredUserDAO;
import ua.cn.stu.rental.dao.hibernate.UserRoleDAO;
import ua.cn.stu.rental.domain.RegisteredUser;
import ua.cn.stu.rental.services.IRegisteredUserService;
import ua.cn.stu.rental.services.impl.RegisteredUserService;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class RegisteredUserServiceTest extends BasicTest{
    private static IRegisteredUserService service;
    private static IRegisteredUserDAO mockDAO;
    private static IUserRoleDAO mockRoleDAO;
    private RegisteredUser registeredUser;

    @BeforeClass
    public static void init() {
        mockDAO = mock(RegisteredUserDAO.class);
        service = new RegisteredUserService(mockDAO);
        mockRoleDAO = mock(UserRoleDAO.class);
    }

    @AfterClass
    public static void free() {
        mockDAO = null;
        service = null;
    }

    @Before
    public void setUp() {
        registeredUser = getRegisteredUser();
    }

    @Test
    public void testAdd() throws Exception {

        when(mockDAO.add(any(RegisteredUser.class))).thenReturn(1);
        registeredUser.setId(1);
        when(mockDAO.find(any(Integer.class))).thenReturn(registeredUser);

        RegisteredUser test = getRegisteredUser();
        RegisteredUser add = service.add(test);
        assertNotNull(add);
        assertEquals(registeredUser.getId(), add.getId());
    }

    @Test
    public void testUpdate() throws Exception {
        RegisteredUser edit = getRegisteredUser();
        edit.setLogin("editLogin");
        when(mockDAO.edit(any(RegisteredUser.class))).thenReturn(edit);

        RegisteredUser testEdit = service.edit(registeredUser);
        assertNotNull(testEdit);
        assertEquals("editLogin", testEdit.getLogin());
    }

    @Test
    public void testDelete() throws Exception {
        doNothing().when(mockDAO).delete(any(RegisteredUser.class));
        when(mockDAO.find(1)).thenReturn(null);

        service.delete(registeredUser);
        RegisteredUser find = mockDAO.find(1);
        assertNull(find);
    }

    @Test
    public void testRegister() throws Exception {
        when(mockDAO.add(any(RegisteredUser.class))).thenReturn(1);
        registeredUser.setId(1);
        when(mockDAO.find(1)).thenReturn(registeredUser);

        RegisteredUser testUser = service.register(registeredUser);

        assertNotNull(testUser);
        assertEquals(1, testUser.getId());
        assertEquals("login", testUser.getLogin());
    }
}
