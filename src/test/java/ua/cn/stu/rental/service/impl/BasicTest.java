package ua.cn.stu.rental.service.impl;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import ua.cn.stu.rental.domain.*;

import java.util.Date;

@ContextConfiguration(locations = "file:src/main/webapp/WEB-INF/persistence-ua.cn.stu.rental.ws.beans.xml")
public abstract class BasicTest extends AbstractJUnit4SpringContextTests{
    protected UserRole getUserRole() {
        UserRole userRole = new UserRole();
        userRole.setDescription("dessc");
        userRole.setName("name");

        return userRole;
    }

    protected Advertisement getAdvertisement() {
        Advertisement advertisement = new Advertisement();
        advertisement.setAddress("adr");
        advertisement.setContactInformation("contInf");
        advertisement.setDescription("descr");
        advertisement.setEntryDate(new Date());
        advertisement.setPrice(100);
        advertisement.setSquare(1L);
        advertisement.setTitle("title");
        advertisement.setWeight(1);

        MeasurementType mesType = getMeasurementType();
        advertisement.setMeasurementType(mesType);

        PlaceType place = getPlaceType();
        advertisement.setPlace(place);

        RegisteredUser user = getRegisteredUser();

        UserRole role = getUserRole();
        user.setRole(role);
        advertisement.setUser(user);
        return advertisement;
    }

    protected PlaceType getPlaceType() {
        PlaceType place = new PlaceType();
        place.setName("place");
        return place;
    }

    protected MeasurementType getMeasurementType() {
        MeasurementType mesType = new MeasurementType();
        mesType.setName("type");
        return mesType;
    }

    protected RegisteredUser getRegisteredUser() {
        RegisteredUser user = new RegisteredUser();
        user.setEmail("email@email.ua");
        user.setIsExtended(false);
        user.setLogin("login");
        user.setPassword("pass");
        return user;
    }
}
