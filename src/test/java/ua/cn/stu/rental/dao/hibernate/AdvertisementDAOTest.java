package ua.cn.stu.rental.dao.hibernate;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import ua.cn.stu.rental.dao.IAdvertisementDAO;
import ua.cn.stu.rental.domain.*;

import java.util.*;

import static org.junit.Assert.*;

/**
 *
 */
//@ContextConfiguration(locations = "file:src/main/webapp/WEB-INF/persistence-ua.cn.stu.rental.ws.beans.xml")
public class AdvertisementDAOTest extends BasicTest {

    public static final String UPDATE_INFO_CHECK = "changedDescr";
    @Autowired
    private IAdvertisementDAO dao;

    protected static String ADD_AD_SCRIPT = "src/main/resources/sql/test_filter_ad.sql";

    @Test
    public void testAdd() throws Exception{
        int oldSize = dao.list().size();

        dao.add(getAdvertisement());

        int newSize = dao.list().size();
        assertTrue(oldSize < newSize);
    }

    @Test
    public void testEdit() throws Exception{
        Advertisement advertisement = getAdvertisement();
        dao.add(advertisement);

        advertisement.setDescription(UPDATE_INFO_CHECK);
        dao.edit(advertisement);

        Advertisement advertisementUpdated = dao.find(advertisement.getId());

        assertEquals(UPDATE_INFO_CHECK, advertisementUpdated.getDescription());
    }

    @Test
    public void testDelete() throws Exception {
        Advertisement advert = getAdvertisement();
        dao.add(advert);

        dao.delete(advert);
        Advertisement checkDelete = dao.find(advert.getId());
        assertNull(checkDelete);
    }

    @Test
    public void testFind() throws Exception {
        Advertisement advertisement = getAdvertisement();
        dao.add(advertisement);

        Advertisement findAdver = dao.find(advertisement.getId());

        assertTrue(findAdver.getId() == advertisement.getId());
    }

    @Test
    public void testList() throws Exception {
        Advertisement ad1 = getAdvertisement();
        Advertisement ad2 = getAdvertisement();
        List<Advertisement> list = new LinkedList<Advertisement>();
        list.add(ad1);
        list.add(ad2);

        dao.add(ad1);
        dao.add(ad2);

        assertTrue(list.size() == dao.list().size());
    }

    @Test
    public void testGetAbByUser() throws Exception {
        Advertisement ad1 = getAdvertisement();
        Advertisement ad2 = getAdvertisement();
        Advertisement ad3 = getAdvertisement();

        RegisteredUser registeredUser = getRegisteredUser();
        RegisteredUser registeredUser1 = getRegisteredUser();
        registeredUser1.setEmail("another@email.com");

        ad1.setUser(registeredUser);
        ad2.setUser(registeredUser);
        ad3.setUser(registeredUser1);

        dao.add(ad1);
        dao.add(ad2);
        dao.add(ad3);

        Map<String, Object> params1 = new HashMap<String, Object>();
        params1.put("userid", registeredUser.getId());

        Map<String, Object> params2 = new HashMap<String, Object>();
        params2.put("userid", registeredUser1.getId());
        List<Advertisement> list1 = dao.getAbByUser(params1, null, 0, 100);
        List<Advertisement> list2 = dao.getAbByUser(params2, null, 0, 100);

        assertTrue(2 == list1.size());
        assertTrue(1 == list2.size());
    }

    @Test
    public void testGetFilteredAd() throws Exception {
        ScriptUtils.executeSqlScript(dataSource.getConnection(), new FileSystemResource(ADD_AD_SCRIPT));

        Advertisement ad1 = dao.find(1000);
        Advertisement ad2 = dao.find(1001);

        Map<String, Object> params1 = new HashMap<String, Object>();
        params1.put("lowprice", 1);
        params1.put("highprice", 100);
        params1.put("title", "%title%");

        /*Map<String, List<Object>> arrayParams = new HashMap<String, List<Object>>();
        List<Object> mesList = new ArrayList<Object>();
        mesList.add(ad1.getMeasurementType().getId());
        mesList.add(ad2.getMeasurementType().getId());
        arrayParams.put("mestype", mesList);
        List<Object> place = new ArrayList<Object>();
        place.add(ad1.getPlace().getId());
        arrayParams.put("place", place);

        List<Advertisement> list = dao.getFilteredAd(params1, arrayParams, 0, 100);*/
        List<Advertisement> list = dao.getFilteredAd(params1, null, 0, 100);

        assertTrue(2 == list.size());

        Map<String, Object> params2 = new HashMap<String, Object>();
        /*params2.put("mestype", ad2.getMeasurementType().getId());*/
        /*params2.put("place", ad2.getPlace().getId());*/
        params2.put("lowprice", 0);
        params2.put("highprice", 50);
        params2.put("title", "%title1%");

        list = dao.getFilteredAd(params2, null, 0, 100);
        assertFalse(1 == list.size());
    }
}
