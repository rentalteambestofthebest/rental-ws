package ua.cn.stu.rental.dao.hibernate;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.cn.stu.rental.dao.IPlaceTypeDAO;
import ua.cn.stu.rental.domain.PlaceType;

import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class PlaceTypeDAOTest extends BasicTest {

    @Autowired
    private IPlaceTypeDAO dao;

    @Test
    public void testAdd() throws Exception {
        int oldSize = dao.list().size();
        PlaceType type = getPlaceType();
        dao.add(type);

        int newSize = dao.list().size();

        assertTrue(oldSize < newSize);
    }

    @Test
    public void testDelete() throws Exception {
        PlaceType type = getPlaceType();
        dao.add(type);

        dao.delete(type);

        PlaceType find = dao.find(type.getId());
        assertNull(find);
    }

    @Test
    public void testUpdate() throws Exception {
        PlaceType type = getPlaceType();
        dao.add(type);

        type.setName("someAnotherName");

        dao.edit(type);

        PlaceType find = dao.find(type.getId());

        assertEquals(type.getName(), find.getName());
    }

    @Test
    public void testFind() throws Exception {
        PlaceType type = getPlaceType();
        dao.add(type);

        PlaceType find = dao.find(type.getId());

        assertNotNull(find);
    }

    @Test
    public void testList() throws Exception {
        PlaceType pt1 = getPlaceType();
        PlaceType pt2 = getPlaceType();

        dao.add(pt1);
        dao.add(pt2);

        List<PlaceType> list = dao.list();

        assertTrue(2 == list.size());
    }
}
