package ua.cn.stu.rental.dao.hibernate;

import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.cn.stu.rental.dao.IMeasurementTypeDAO;
import ua.cn.stu.rental.domain.MeasurementType;

import java.util.List;

public class MeasurementDAOTest extends BasicTest {

    @Autowired
    private IMeasurementTypeDAO dao;

    @Test
    public void testAdd() throws Exception {
        int oldSize = dao.list().size();
        MeasurementType measurementType = getMeasurementType();
        dao.add(measurementType);

        int newSize = dao.list().size();

        assertTrue(oldSize < newSize);
    }

    @Test
    public void testDelete() throws Exception {
        MeasurementType measurementType = getMeasurementType();
        dao.add(measurementType);

        dao.delete(measurementType);

        MeasurementType find = dao.find(measurementType.getId());
        assertNull(find);
    }

    @Test
    public void testUpdate() throws Exception {
        MeasurementType type = getMeasurementType();
        dao.add(type);

        type.setName("someAnotherName");

        dao.edit(type);

        MeasurementType find = dao.find(type.getId());

        assertEquals(type.getName(), find.getName());
    }

    @Test
    public void testFind() throws Exception {
        MeasurementType type = getMeasurementType();
        dao.add(type);

        MeasurementType find = dao.find(type.getId());

        assertNotNull(find);
    }

    @Test
    public void testList() throws Exception {
        MeasurementType mt1 = getMeasurementType();
        MeasurementType mt2 = getMeasurementType();

        dao.add(mt1);
        dao.add(mt2);

        List<MeasurementType> list = dao.list();

        assertTrue(2 == list.size());
    }
}
