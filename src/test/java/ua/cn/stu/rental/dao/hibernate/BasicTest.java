package ua.cn.stu.rental.dao.hibernate;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import ua.cn.stu.rental.domain.*;

import java.util.Date;

/**
 *
 */
@ContextConfiguration(locations = "file:src/main/webapp/WEB-INF/persistence-beans.xml")
public abstract  class BasicTest extends AbstractJUnit4SpringContextTests {

    protected static String DELETE_SCRIPT = "src/main/resources/sql/cleanup.sql";

    @Autowired
    protected DriverManagerDataSource dataSource;

    @Before
    @After
    public void cleanDataBase() throws Exception {
        ScriptUtils.executeSqlScript(dataSource.getConnection(), new FileSystemResource(DELETE_SCRIPT));
    }

    protected UserRole getUserRole() {
        UserRole userRole = new UserRole();
        userRole.setDescription("dessc");
        userRole.setName("name");

        return userRole;
    }

    protected Advertisement getAdvertisement() {
        Advertisement advertisement = new Advertisement();
        advertisement.setAddress("adr");
        advertisement.setContactInformation("contInf");
        advertisement.setDescription("descr");
        advertisement.setEntryDate(new Date());
        advertisement.setPrice(100);
        advertisement.setSquare(1L);
        advertisement.setTitle("title");
        advertisement.setWeight(1);

        MeasurementType mesType = getMeasurementType();
        advertisement.setMeasurementType(mesType);

        PlaceType place = getPlaceType();
        advertisement.setPlace(place);

        RegisteredUser user = getRegisteredUser();

        UserRole role = getUserRole();
        user.setRole(role);
        advertisement.setUser(user);
        return advertisement;
    }

    protected PlaceType getPlaceType() {
        PlaceType place = new PlaceType();
        place.setName("place");
        return place;
    }

    protected MeasurementType getMeasurementType() {
        MeasurementType mesType = new MeasurementType();
        mesType.setName("type");
        return mesType;
    }

    protected RegisteredUser getRegisteredUser() {
        RegisteredUser user = new RegisteredUser();
        user.setEmail("email@email.ua");
        user.setIsExtended(false);
        user.setLogin("login");
        user.setPassword("pass");
        return user;
    }
}
