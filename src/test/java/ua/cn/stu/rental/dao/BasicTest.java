package ua.cn.stu.rental.dao;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "file:/src/main/WEB-INF/persistence-beans.xml")
public class BasicTest extends AbstractJUnit4SpringContextTests {

    protected static String DELETE_SCRIPT = "src/main/resources/sql/cleanup.sql";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Before
    public void cleanDataBaseBeforeTest() {
        JdbcTestUtils.executeSqlScript(jdbcTemplate, new FileSystemResource(DELETE_SCRIPT), false);
    }
}
