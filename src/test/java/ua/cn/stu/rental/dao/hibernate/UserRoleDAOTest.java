package ua.cn.stu.rental.dao.hibernate;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.cn.stu.rental.dao.IUserRoleDAO;
import ua.cn.stu.rental.domain.UserRole;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class UserRoleDAOTest extends BasicTest {

    @Autowired
    private IUserRoleDAO dao;

    @Test
    public void testAdd() throws Exception {
        int oldSize = dao.list().size();
        UserRole type = getUserRole();
        dao.add(type);

        int newSize = dao.list().size();

        assertTrue(oldSize < newSize);
    }

    @Test
    public void testDelete() throws Exception {
        UserRole role = getUserRole();
        dao.add(role);

        dao.delete(role);

        UserRole find = dao.find(role.getId());
        assertNull(find);
    }

    @Test
    public void testUpdate() throws Exception {
        UserRole role = getUserRole();
        dao.add(role);

        role.setName("someAnotherName");

        dao.edit(role);

        UserRole find = dao.find(role.getId());

        assertEquals(role.getName(), find.getName());
    }

    @Test
    public void testFind() throws Exception {
        UserRole role = getUserRole();
        dao.add(role);

        UserRole find = dao.find(role.getId());

        assertNotNull(find);
    }

    @Test
    public void testList() throws Exception {
        UserRole ur1 = getUserRole();
        UserRole ur2 = getUserRole();

        dao.add(ur1);
        dao.add(ur2);

        List<UserRole> list = dao.list();

        assertTrue(2 == list.size());
    }

    @Test
    public void testGetRoleByName() throws Exception {
        UserRole role = getUserRole();
        role.setName("user");

        dao.add(role);

        Map<String, Object> params = new HashMap<>();
        params.put("name", "user");

        UserRole roleByName = dao.getUserRoleByName(params);

        assertNotNull(roleByName);
        assertEquals("user", roleByName.getName());
    }
}
