package ua.cn.stu.rental.dao.hibernate;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ua.cn.stu.rental.dao.IRegisteredUserDAO;
import ua.cn.stu.rental.domain.RegisteredUser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class RegisteredUserDAOTest extends BasicTest {

    @Autowired
    private IRegisteredUserDAO dao;

    @Test
    public void testAdd() throws Exception {
        RegisteredUser registeredUser = getRegisteredUser();
        int oldSize = dao.list().size();
        dao.add(registeredUser);
        int newSize = dao.list().size();

        assertTrue(oldSize < newSize);

    }

    @Test
    public void testDelete() throws Exception {
        RegisteredUser registeredUser = getRegisteredUser();
        dao.add(registeredUser);
        dao.delete(registeredUser);
        RegisteredUser findUser = dao.find(registeredUser.getId());
        assertNull(findUser);
    }

    @Test
    public void testUpdate() throws Exception {
        RegisteredUser registeredUser = getRegisteredUser();
        dao.add(registeredUser);

        registeredUser.setEmail("some@email.com123");
        dao.edit(registeredUser);

        RegisteredUser findUser = dao.find(registeredUser.getId());

        assertEquals(registeredUser.getEmail(), findUser.getEmail());
    }

    @Test
    public void testFind() throws Exception {
        RegisteredUser registeredUser = getRegisteredUser();
        dao.add(registeredUser);

        RegisteredUser findUser = dao.find(registeredUser.getId());

        assertNotNull(findUser);
    }

    @Test
    public void testList() throws Exception {
        RegisteredUser ru1 = getRegisteredUser();
        RegisteredUser ru2 = getRegisteredUser();

        dao.add(ru1);
        dao.add(ru2);

        List<RegisteredUser> list = dao.list();

        assertTrue(2 == list.size());
    }

    @Test
    public void testGetUserByCredential() throws Exception {
        RegisteredUser registeredUser = getRegisteredUser();
        dao.add(registeredUser);

        Map<String, Object> params = new HashMap<>();
        params.put("login", registeredUser.getLogin());
        params.put("pass", registeredUser.getPassword());

        RegisteredUser savedUser = dao.getUserByCredential(params, 0, 100);

        assertEquals(registeredUser, savedUser);
    }
}