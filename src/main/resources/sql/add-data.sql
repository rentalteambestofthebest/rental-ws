delete FROM advertisement;
delete from measurementtype;
DELETE FROM placetype;
DELETE FROM userimage;
DELETE FROM registereduser;
DELETE FROM userrole;



insert into userrole VALUES (1,               -- id
                             'role1',         -- role
                             'admin' );       -- name
insert into userrole VALUES (2, 'role2', 'user');
insert into userrole VALUES (3, 'role3', 'moderator');

insert into registereduser VALUES (1,               -- id
                                   'root@root.mil', -- email
                                   false,           -- isExtended
                                   'god',           -- login
                                   '111',           -- password
                                   1);              -- role
insert into registereduser VALUES (2, 'user1@user.ua', false, 'user1', '321', 2);
insert into registereduser VALUES (3, 'user2@user.ua', true, 'user2', '321', 2);
insert into registereduser VALUES (4, 'moder2@user.ua', true, 'moder', '321', 3);

insert into placetype VALUES (1,        -- id
                              'house'); -- name
insert into placetype VALUES (2, 'apartment');

INSERT INTO measurementtype VALUES (1,      -- id
                                    'cm');  -- name
INSERT INTO measurementtype VALUES (2, 'm');

insert into advertisement VALUES (1,            -- id
                                  'address1',   -- address
                                  'skype',      -- contact info
                                  'desc1',      -- description
                                  current_date, -- entry date
                                  100,          -- price
                                  10,           -- square
                                  'title1',     -- title
                                  1,            -- weight
                                  2,            -- measurement type
                                  2,            -- place type
                                  2);           -- user
--insert into advertisement VALUES (1, 'address1', 'skype', 'desc1', current_date, 100, 10, 'title1', 1, 2, 2, 2);
