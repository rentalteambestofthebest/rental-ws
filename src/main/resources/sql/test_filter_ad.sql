  INSERT INTO userrole(id, name, description) VALUES (1, 'role', 'user');

  INSERT INTO registereduser(id, email, isextended, login, password, role_id)
    VALUES (1, 'moder2@user.ua', TRUE, 'moder', '321', 1);

  INSERT INTO measurementtype(id, name) VALUES (1, 'm');
  INSERT INTO measurementtype(id, name) VALUES (2, 'cm');
  insert into placetype(id, name) VALUES (1, 'apartment');

  INSERT INTO advertisement(id, address, contactinformation, description, entrydate, price, square, title, weight, measurementtype_id, place_id, user_id) VALUES (
                                    1000,
                                    'address1', -- address
                                    'skype', -- contact info
                                    'desc1', -- description
                                    current_date, -- entry date
                                    100, -- price
                                    10, -- square
                                    'title1', -- title
                                    1, -- weight
                                    1, -- measurement type
                                    1, -- place type
                                    1);

  insert into advertisement(id, address, contactinformation, description, entrydate, price, square, title, weight, measurementtype_id, place_id, user_id)
  VALUES (1001, 'address1', 'skype', 'desc1', current_date, 100, 10, 'title2', 1, 2, 1, 1);