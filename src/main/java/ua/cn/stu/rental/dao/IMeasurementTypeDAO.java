package ua.cn.stu.rental.dao;

import ua.cn.stu.rental.domain.MeasurementType;

/**
 * Created by victor on 12/3/14.
 */
public interface IMeasurementTypeDAO extends IGenericDAO<MeasurementType> {
}
