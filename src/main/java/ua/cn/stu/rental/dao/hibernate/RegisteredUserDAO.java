package ua.cn.stu.rental.dao.hibernate;

import org.springframework.stereotype.Repository;
import ua.cn.stu.rental.dao.IRegisteredUserDAO;
import ua.cn.stu.rental.domain.RegisteredUser;

import java.util.List;
import java.util.Map;

@Repository
public class RegisteredUserDAO extends HibernateDAO<RegisteredUser> implements IRegisteredUserDAO {
    @Override
    public RegisteredUser getUserByCredential(Map<String, Object> params, int pageIndex, int pageSize) throws Exception {
        List<RegisteredUser> list = executeQuery("getUserByCredential", params, null, pageIndex, pageSize);
        return list != null && list.size() == 1 ? list.get(0) : new RegisteredUser();
    }
}
