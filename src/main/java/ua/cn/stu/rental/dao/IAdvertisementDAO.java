package ua.cn.stu.rental.dao;

import ua.cn.stu.rental.domain.Advertisement;

import java.util.List;
import java.util.Map;

/**
 *
 */
public interface IAdvertisementDAO extends IGenericDAO<Advertisement> {
    public List<Advertisement> getAbByUser(Map<String, Object> params, Map<String, List<Object>> arrayParams,
                                           int pageIndex, int pageSize) throws Exception;

    public List<Advertisement> getFilteredAd(Map<String, Object> filter, Map<String, List<Object>> arrayParams,
                                             int pageIndex, int pageSize) throws Exception;
}
