package ua.cn.stu.rental.dao.hibernate;

import org.springframework.stereotype.Repository;
import ua.cn.stu.rental.dao.IUserRoleDAO;
import ua.cn.stu.rental.domain.UserRole;

import java.util.List;
import java.util.Map;

@Repository
public class UserRoleDAO extends HibernateDAO<UserRole> implements IUserRoleDAO {
    @Override
    public UserRole getUserRoleByName(Map<String, Object> params) throws Exception{
        List<UserRole> roles = executeQuery("getUserRoleByName", params, null, 0, 1);
        return roles != null && !roles.isEmpty()? roles.get(0) : new UserRole();
    }
}
