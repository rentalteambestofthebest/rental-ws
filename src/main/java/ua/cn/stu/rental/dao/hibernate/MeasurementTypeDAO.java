package ua.cn.stu.rental.dao.hibernate;

import org.springframework.stereotype.Repository;
import ua.cn.stu.rental.dao.IMeasurementTypeDAO;
import ua.cn.stu.rental.domain.MeasurementType;

@Repository
public class MeasurementTypeDAO extends HibernateDAO<MeasurementType> implements IMeasurementTypeDAO{
}
