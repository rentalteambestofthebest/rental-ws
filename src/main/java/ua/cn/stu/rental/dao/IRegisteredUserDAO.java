package ua.cn.stu.rental.dao;

import ua.cn.stu.rental.domain.RegisteredUser;

import java.util.Map;

/**
 *
 */
public interface IRegisteredUserDAO extends IGenericDAO<RegisteredUser> {
    public RegisteredUser getUserByCredential(Map<String, Object> params, int pageIndex, int pageSize) throws Exception;
}
