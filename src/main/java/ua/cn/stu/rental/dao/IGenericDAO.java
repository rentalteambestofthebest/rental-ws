package ua.cn.stu.rental.dao;

import ua.cn.stu.rental.domain.DomainSuperClass;

import java.util.List;
import java.util.Map;

/**
 *
 */
public interface IGenericDAO<T extends DomainSuperClass> {
    public int add(T add) throws Exception;
    public void delete(T del) throws  Exception;
    public T edit(T edit) throws Exception;
    public List<T> list() throws Exception;
    public T find(int id) throws Exception;
    public List<T> executeQuery(String namedQuery,
                                   Map<String, Object> params,
                                   final Map<String, List<Object>> arrayParams,
                                   int pageIndex,
                                   int pageSize) throws Exception;
}
