package ua.cn.stu.rental.dao.hibernate;

import org.springframework.stereotype.Repository;
import ua.cn.stu.rental.dao.IPlaceTypeDAO;
import ua.cn.stu.rental.domain.PlaceType;

@Repository
public class PlaceTypeDAO extends HibernateDAO<PlaceType> implements IPlaceTypeDAO {
}
