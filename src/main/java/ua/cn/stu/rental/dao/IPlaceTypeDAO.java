package ua.cn.stu.rental.dao;

import ua.cn.stu.rental.domain.PlaceType;

/**
 * Created by victor on 12/3/14.
 */
public interface IPlaceTypeDAO extends IGenericDAO<PlaceType> {
}
