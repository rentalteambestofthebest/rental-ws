package ua.cn.stu.rental.dao.hibernate;

import org.springframework.stereotype.Repository;
import ua.cn.stu.rental.dao.IAdvertisementDAO;
import ua.cn.stu.rental.domain.Advertisement;

import java.util.List;
import java.util.Map;

/**
 *
 */
@Repository
public class AdvertisementDAO extends HibernateDAO<Advertisement> implements IAdvertisementDAO {
    @Override
    public List<Advertisement> getAbByUser(Map<String, Object> params, Map<String, List<Object>> arrayParams,
                                           int pageIndex, int pageSize) throws Exception {
        return executeQuery("getAdByUser", params, arrayParams, pageIndex, pageSize);
    }

    @Override
    public List<Advertisement> getFilteredAd(Map<String, Object> filter, Map<String, List<Object>> arrayParams,
                                             int pageIndex, int pageSize) throws Exception {
        return executeQuery("getFilteredAd", filter, arrayParams, pageIndex, pageSize);
    }
}
