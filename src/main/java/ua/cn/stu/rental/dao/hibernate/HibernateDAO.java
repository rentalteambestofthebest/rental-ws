package ua.cn.stu.rental.dao.hibernate;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ua.cn.stu.rental.dao.IGenericDAO;
import ua.cn.stu.rental.domain.DomainSuperClass;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

/**
 *
 */

@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
public abstract  class HibernateDAO<T extends DomainSuperClass> implements IGenericDAO<T> {

    private SessionFactory sessionFactory;

    protected Class<? extends T> type;

    public HibernateDAO() {
        type = (Class<T>) ((ParameterizedType) getClass().
                getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public int add(T add) throws Exception {
        return (int) getCurrentSession().save(add);
    }

    @Override
    public void delete(T del) throws Exception {
        getCurrentSession().delete(del);
    }

    @Override
    public T edit(T edit) throws Exception {
        getCurrentSession().update(edit);
        return edit;
    }

    @Override
    public List<T> list() throws Exception {
        return getCurrentSession().createCriteria(type).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    @Override
    public T find(int id) throws Exception {
        return (T) getCurrentSession().get(type, id);
    }

    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<T> executeQuery(String namedQuery,
                                     Map<String, Object> params,
                                     final Map<String, List<Object>> arrayParams,
                                     int pageIndex,
                                     int pageSize)
            throws Exception {
        List<T> result;
        try {
            Query q = getCurrentSession().getNamedQuery(namedQuery);
            q.setFirstResult(pageIndex * pageSize);
            q.setMaxResults(pageSize);
            for (String key : params.keySet()) {
                q.setParameter(key, params.get(key));
            }
            if (arrayParams != null) {
                for (String key: arrayParams.keySet()) {
                    q.setParameterList(key, arrayParams.get(key));
                }
            }
            result = q.list();
        } catch (Exception e) {
            throw new Exception(e);
        }
        return result;
    }
}
