package ua.cn.stu.rental.dao;

import ua.cn.stu.rental.domain.UserRole;

import java.util.Map;

/**
 *
 */
public interface IUserRoleDAO extends IGenericDAO<UserRole> {
    public UserRole getUserRoleByName(Map<String, Object> params) throws Exception;
}
