package ua.cn.stu.rental.domain;

@javax.persistence.Entity
public class PlaceType extends DomainSuperClass {

	@javax.persistence.Column(nullable = false) 
	protected String name;
	public PlaceType(){
		super();
	}
	public String getName() {
		return this.name;	
	}
	public void setName(String myName) {
		this.name = myName;	
	}
	public void unsetName() {
		this.name = "";	
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		PlaceType placeType = (PlaceType) o;

		if (name != null ? !name.equals(placeType.name) : placeType.name != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return name != null ? name.hashCode() : 0;
	}
}

