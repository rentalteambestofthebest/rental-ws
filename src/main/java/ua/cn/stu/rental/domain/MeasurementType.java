package ua.cn.stu.rental.domain;


@javax.persistence.Entity
public class MeasurementType extends DomainSuperClass {

	@javax.persistence.Column(nullable = false) 
	protected String name;
	public MeasurementType(){
		super();
	}
	public String getName() {
		return this.name;	
	}
	public void setName(String myName) {
		this.name = myName;	
	}
	public void unsetName() {
		this.name = "";	
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MeasurementType that = (MeasurementType) o;

		if (name != null ? !name.equals(that.name) : that.name != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return name != null ? name.hashCode() : 0;
	}
}

