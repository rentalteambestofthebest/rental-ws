package ua.cn.stu.rental.domain;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@NamedQueries({
		@NamedQuery(name = "getUserRoleByName",
				query = "select x from UserRole x where x.name like :name")
})
@javax.persistence.Entity
public class UserRole extends DomainSuperClass {
	@javax.persistence.Column(nullable = false)
	protected String name;
	@javax.persistence.Column(nullable = false)
	protected String description;
	public UserRole(){
		super();
	}
	public String getName() {
		return this.name;	
	}
	public String getDescription() {
		return this.description;	
	}
	public void setName(String myName) {
		this.name = myName;	
	}
	public void setDescription(String myDescription) {
		this.description = myDescription;	
	}
	public void unsetName() {
		this.name = "";	
	}
	public void unsetDescription() {
		this.description = "";	
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		UserRole userRole = (UserRole) o;

		if (description != null ? !description.equals(userRole.description) : userRole.description != null)
			return false;
		if (name != null ? !name.equals(userRole.name) : userRole.name != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (description != null ? description.hashCode() : 0);
		return result;
	}
}

