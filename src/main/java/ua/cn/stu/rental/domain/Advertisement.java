package ua.cn.stu.rental.domain;
import javax.persistence.*;
import java.util.Set;
import java.util.HashSet;
import java.util.Date;
/*
@NamedQuery(name = "getFilteredAd",
		 		 query = "select x from Advertisement x where " +
						 "(x.measurementType.id in (:mestype) " +
						 "and x.place.id in (:place)" +
						 "and (x.price >= :lowprice and x.price <= :highprice)" +
						 "and x.title like :title)")
*/
 @NamedQueries({
		 @NamedQuery(name = "getAdByUser",
				 query = "select x FROM Advertisement x where (x.user.id = :userid)"),
		 @NamedQuery(name = "getFilteredAd",
		 		 query = "select x from Advertisement x where " +
						 "((x.price >= :lowprice and x.price <= :highprice)" +
						 "and x.title like :title)")

 })
@javax.persistence.Entity
@Table(name = "advertisement")
public class Advertisement extends DomainSuperClass {
	@javax.persistence.Column(nullable = false)
	protected String title;

	@javax.persistence.Column(nullable = false)
	protected int price;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "advertisement", cascade = CascadeType.ALL)
	protected Set<UserImage> images;

	@javax.persistence.Column(nullable = false) 
	protected String address;

	@javax.persistence.Column(nullable = false) 
	protected String contactInformation;

	@javax.persistence.Column(nullable = false) 
	protected long square;

	@javax.persistence.OneToOne(cascade = CascadeType.ALL)
	protected MeasurementType measurementType;

	@javax.persistence.OneToOne(cascade = CascadeType.ALL)
	protected PlaceType place;

	@javax.persistence.Temporal(javax.persistence.TemporalType.DATE) 
	@javax.persistence.Column(nullable = false) 
	protected Date entryDate;

	@javax.persistence.Column(nullable = false) 
	protected String description;

	@javax.persistence.Column(nullable = false) 
	protected int weight;

	@javax.persistence.OneToOne(cascade = CascadeType.ALL)
	protected RegisteredUser user;

	public Advertisement(){
		super();
	}

	public String getTitle() {
		return this.title;	
	}
	public int getPrice() {
		return this.price;	
	}
	public Set<UserImage> getImages() {
		if(this.images == null) {
				this.images = new HashSet<UserImage>();
		}
		return (Set<UserImage>) this.images;	
	}
	public String getAddress() {
		return this.address;	
	}
	public String getContactInformation() {
		return this.contactInformation;	
	}
	public long getSquare() {
		return this.square;	
	}
	public MeasurementType getMeasurementType() {
		return this.measurementType;	
	}
	public PlaceType getPlace() {
		return this.place;	
	}
	public Date getEntryDate() {
		return this.entryDate;	
	}
	public String getDescription() {
		return this.description;	
	}
	public int getWeight() {
		return this.weight;	
	}
	public RegisteredUser getUser() {
		return this.user;	
	}
	public void addAllImages(Set<UserImage> newImages) {
		if (this.images == null) {
			this.images = new HashSet<UserImage>();
		}
		this.images.addAll(newImages);	
	}
	public void removeAllImages(Set<UserImage> newImages) {
		if(this.images == null) {
			return;
		}
		
		this.images.removeAll(newImages);	
	}
	public void setTitle(String myTitle) {
		this.title = myTitle;	
	}
	public void setPrice(int myPrice) {
		this.price = myPrice;	
	}
	public void addImages(UserImage newImages) {
		if(this.images == null) {
			this.images = new HashSet<UserImage>();
		}
		
		this.images.add(newImages);	
	}
	public void setAddress(String myAddress) {
		this.address = myAddress;	
	}
	public void setContactInformation(String myContactInformation) {
		this.contactInformation = myContactInformation;	
	}
	public void setSquare(long mySquare) {
		this.square = mySquare;	
	}
	public void setMeasurementType(MeasurementType myMeasurementType) {
		this.measurementType = myMeasurementType;	
	}
	public void setPlace(PlaceType myPlace) {
		this.place = myPlace;	
	}
	public void setEntryDate(Date myEntryDate) {
		this.entryDate = myEntryDate;	
	}
	public void setDescription(String myDescription) {
		this.description = myDescription;	
	}
	public void setWeight(int myWeight) {
		this.weight = myWeight;	
	}
	public void setUser(RegisteredUser myUser) {
		this.user = myUser;	
	}
	public void unsetTitle() {
		this.title = "";	
	}
	public void unsetPrice() {
		this.price = 0;	
	}
	public void removeImages(UserImage oldImages) {
		if(this.images == null)
			return;
		
		this.images.remove(oldImages);	
	}
	public void unsetAddress() {
		this.address = "";	
	}
	public void unsetContactInformation() {
		this.contactInformation = "";	
	}
	public void unsetSquare() {
		this.square = 0L;	
	}
	public void unsetMeasurementType() {
		this.measurementType = new MeasurementType();	
	}
	public void unsetPlace() {
		this.place = new PlaceType();	
	}
	public void unsetEntryDate() {
		this.entryDate = new Date();	
	}
	public void unsetDescription() {
		this.description = "";	
	}
	public void unsetWeight() {
		this.weight = 0;	
	}
	public void unsetUser() {
		this.user = new RegisteredUser();	
	}

	 @Override
	 public boolean equals(Object o) {
		 if (this == o) return true;
		 if (o == null || getClass() != o.getClass()) return false;
		 if (!super.equals(o)) return false;

		 Advertisement that = (Advertisement) o;

		 if (price != that.price) return false;
		 if (square != that.square) return false;
		 if (weight != that.weight) return false;
		 if (address != null ? !address.equals(that.address) : that.address != null) return false;
		 if (contactInformation != null ? !contactInformation.equals(that.contactInformation) : that.contactInformation != null)
			 return false;
		 if (description != null ? !description.equals(that.description) : that.description != null) return false;
		 if (entryDate != null ? !entryDate.equals(that.entryDate) : that.entryDate != null) return false;
		 if (images != null ? !images.equals(that.images) : that.images != null) return false;
		 if (measurementType != null ? !measurementType.equals(that.measurementType) : that.measurementType != null)
			 return false;
		 if (place != null ? !place.equals(that.place) : that.place != null) return false;
		 if (title != null ? !title.equals(that.title) : that.title != null) return false;
		 if (user != null ? !user.equals(that.user) : that.user != null) return false;

		 return true;
	 }

	 @Override
	 public int hashCode() {
		 int result = super.hashCode();
		 result = 31 * result + (title != null ? title.hashCode() : 0);
		 result = 31 * result + price;
		 result = 31 * result + (images != null ? images.hashCode() : 0);
		 result = 31 * result + (address != null ? address.hashCode() : 0);
		 result = 31 * result + (contactInformation != null ? contactInformation.hashCode() : 0);
		 result = 31 * result + (int) (square ^ (square >>> 32));
		 result = 31 * result + (measurementType != null ? measurementType.hashCode() : 0);
		 result = 31 * result + (place != null ? place.hashCode() : 0);
		 result = 31 * result + (entryDate != null ? entryDate.hashCode() : 0);
		 result = 31 * result + (description != null ? description.hashCode() : 0);
		 result = 31 * result + weight;
		 result = 31 * result + (user != null ? user.hashCode() : 0);
		 return result;
	 }
 }

