package ua.cn.stu.rental.domain;


import javax.persistence.CascadeType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@NamedQueries({
        @NamedQuery(name = "getUserByCredential",
                query = "select x from RegisteredUser x " +
                        "where (x.login like :login and x.password like :pass)")
})
@javax.persistence.Entity
public class RegisteredUser extends DomainSuperClass {

    @javax.persistence.Column(nullable = false)
    protected String login;
    @javax.persistence.Column(nullable = false)
    protected String email;
    @javax.persistence.Column(nullable = false)
    protected String password;
    @javax.persistence.OneToOne(cascade = CascadeType.ALL)
    protected UserRole role;
    @javax.persistence.Column(nullable = false)
    protected boolean isExtended;

    public RegisteredUser() {
        super();
    }

    public String getLogin() {
        return this.login;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPassword() {
        return this.password;
    }

    public UserRole getRole() {
        return this.role;
    }

    public boolean isIsExtended() {
        return this.isExtended;
    }

    public void setLogin(String myLogin) {
        this.login = myLogin;
    }

    public void setEmail(String myEmail) {
        this.email = myEmail;
    }

    public void setPassword(String myPassword) {
        this.password = myPassword;
    }

    public void setRole(UserRole myRole) {
        this.role = myRole;
    }

    public void setIsExtended(boolean myIsExtended) {
        this.isExtended = myIsExtended;
    }

    public void unsetLogin() {
        this.login = "";
    }

    public void unsetEmail() {
        this.email = "";
    }

    public void unsetPassword() {
        this.password = "";
    }

    public void unsetRole() {
        this.role = new UserRole();
    }

    public void unsetIsExtended() {
        this.isExtended = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegisteredUser that = (RegisteredUser) o;

        if (isExtended != that.isExtended) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (role != null ? !role.equals(that.role) : that.role != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = login != null ? login.hashCode() : 0;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (isExtended ? 1 : 0);
        return result;
    }
}

