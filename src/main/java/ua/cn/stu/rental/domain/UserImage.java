package ua.cn.stu.rental.domain;


import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@javax.persistence.Entity
public class UserImage extends DomainSuperClass {
	@javax.persistence.Column(nullable = false)
	protected String name;
	@javax.persistence.Column(nullable = false)
	protected byte[] image;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "advertisement", nullable = false)
	protected Advertisement advertisement;
	public UserImage(){
		super();
	}
	public String getName() {
		return this.name;	
	}
	public byte[] getImage() {
		return this.image;	
	}
	public void setName(String myName) {
		this.name = myName;	
	}
	public void setImage(byte[] myImage) {
		this.image = myImage;	
	}
	public void unsetName() {
		this.name = "";	
	}
	public void unsetImage() {
		this.image = new byte[32];	
	}
}

