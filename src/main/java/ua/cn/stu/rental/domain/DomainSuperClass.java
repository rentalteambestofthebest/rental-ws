package ua.cn.stu.rental.domain;


import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public abstract class DomainSuperClass implements Serializable
{
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@javax.persistence.Id 
	@javax.persistence.Column(nullable = false) 
	protected int id;

	public DomainSuperClass(){
		super();
	}
	public int getId() {
		return this.id;	
	}
	public void setId(int myId) {
		this.id = myId;	
	}
	public void unsetId() {
		this.id = 0;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		DomainSuperClass that = (DomainSuperClass) o;

		if (id != that.id) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return id;
	}
}

