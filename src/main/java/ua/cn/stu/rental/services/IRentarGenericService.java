package ua.cn.stu.rental.services;

import java.util.List;

/**
 *
 */
public interface IRentarGenericService<T extends ua.cn.stu.rental.domain.DomainSuperClass> {
    public T add(T add) throws Exception;
    public T edit(T edit) throws Exception;
    public void delete(T delete) throws Exception;
    public T getById(int id) throws Exception;
    public List<T> list() throws Exception;
}
