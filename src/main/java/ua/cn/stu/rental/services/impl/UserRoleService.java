package ua.cn.stu.rental.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.cn.stu.rental.dao.IUserRoleDAO;
import ua.cn.stu.rental.domain.UserRole;
import ua.cn.stu.rental.services.IUserRoleService;

import java.util.Map;

@Service
public class UserRoleService extends GenericServiceImpl<UserRole> implements IUserRoleService {

    private IUserRoleDAO roleDAO;

    @Autowired
    public UserRoleService(IUserRoleDAO dDAO) {
        super.dao = dDAO;
        this.roleDAO = dDAO;
    }
    @Override
    public UserRole getUserRoleByName(Map<String, Object> params) throws Exception {
        return this.roleDAO.getUserRoleByName(params);
    }
}
