package ua.cn.stu.rental.services.impl;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ua.cn.stu.rental.dao.IGenericDAO;
import ua.cn.stu.rental.domain.DomainSuperClass;
import ua.cn.stu.rental.services.IRentarGenericService;

import java.util.List;

/**
 *
 */
@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
public abstract class GenericServiceImpl<T extends DomainSuperClass> implements IRentarGenericService<T> {

    protected IGenericDAO<T> dao;

    @Override
    public T add(T add) throws Exception {
        dao.add(add);
        return dao.find(add.getId());
    }

    @Override
    public T edit(T edit) throws Exception {
        return dao.edit(edit);
    }

    @Override
    public void delete(T delete) throws Exception {
        dao.delete(delete);
    }

    @Override
    public T getById(int id) throws Exception {
        return dao.find(id);
    }

    @Override
    public List<T> list() throws Exception {
        return dao.list();
    }
}
