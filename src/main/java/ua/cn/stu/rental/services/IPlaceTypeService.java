package ua.cn.stu.rental.services;

import ua.cn.stu.rental.domain.PlaceType;

public interface IPlaceTypeService extends IRentarGenericService<PlaceType> {
}
