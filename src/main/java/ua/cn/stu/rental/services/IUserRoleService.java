package ua.cn.stu.rental.services;

import ua.cn.stu.rental.domain.UserRole;

import java.util.Map;

/**
 * Created by victor on 12/6/14.
 */
public interface IUserRoleService extends IRentarGenericService<UserRole> {
    public UserRole getUserRoleByName(Map<String, Object> params) throws Exception;
}
