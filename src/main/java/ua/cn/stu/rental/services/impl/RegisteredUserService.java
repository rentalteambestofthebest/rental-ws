package ua.cn.stu.rental.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.cn.stu.rental.dao.IRegisteredUserDAO;
import ua.cn.stu.rental.domain.RegisteredUser;
import ua.cn.stu.rental.services.IRegisteredUserService;

import java.util.Map;

/**
 * Created by victor on 12/4/14.
 */
@Service
public class RegisteredUserService extends GenericServiceImpl<RegisteredUser> implements IRegisteredUserService {

    private IRegisteredUserDAO userDAO;

    @Autowired
    public RegisteredUserService (IRegisteredUserDAO dao) {
        super.dao = dao;
        userDAO = dao;
    }
    @Override
    public RegisteredUser register(RegisteredUser user) throws Exception {
        return add(user);
    }

    @Override
    public RegisteredUser authrorize(Map<String, Object> params) throws Exception {

        return userDAO.getUserByCredential(params, 0, 10);
    }
}
