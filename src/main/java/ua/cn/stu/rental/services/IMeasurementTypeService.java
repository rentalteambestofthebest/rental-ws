package ua.cn.stu.rental.services;

import ua.cn.stu.rental.domain.MeasurementType;

public interface IMeasurementTypeService extends IRentarGenericService<MeasurementType> {
}
