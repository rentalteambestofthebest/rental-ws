package ua.cn.stu.rental.services.impl;

import org.springframework.stereotype.Service;
import ua.cn.stu.rental.domain.MeasurementType;
import ua.cn.stu.rental.services.IMeasurementTypeService;

@Service
public class MeasurementTypeService extends GenericServiceImpl<MeasurementType> implements IMeasurementTypeService {
}
