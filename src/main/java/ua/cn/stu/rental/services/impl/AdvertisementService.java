package ua.cn.stu.rental.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.cn.stu.rental.dao.IAdvertisementDAO;
import ua.cn.stu.rental.domain.Advertisement;
import ua.cn.stu.rental.services.IAdvertisementService;

import java.util.List;
import java.util.Map;

@Service
public class AdvertisementService extends GenericServiceImpl<Advertisement> implements IAdvertisementService {

    private IAdvertisementDAO adDao;

    @Autowired
    public AdvertisementService(IAdvertisementDAO adDao) {
        dao = adDao;
        this.adDao = adDao;
    }

    @Override
    public List<Advertisement> getFilteredAds(Map<String, Object> params, Map<String, List<Object>> arrayParams, int pageIndex, int pageSize) throws Exception {
        return this.adDao.getFilteredAd(params, arrayParams, pageIndex, pageSize);
    }

    @Override
    public Advertisement createAd(Advertisement ad) throws Exception {
        return add(ad);
    }

    @Override
    public List<Advertisement> getAdsByUser(Map<String, Object> params, Map<String, List<Object>> arrayParams, int pageIndex, int pageSize) throws Exception {
        return this.adDao.getAbByUser(params, arrayParams, pageIndex, pageSize);
    }
}
