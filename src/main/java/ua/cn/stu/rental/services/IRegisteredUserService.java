package ua.cn.stu.rental.services;

import ua.cn.stu.rental.domain.RegisteredUser;

import java.util.Map;

public interface IRegisteredUserService extends IRentarGenericService<RegisteredUser> {
    public RegisteredUser register(RegisteredUser user) throws  Exception;
    public RegisteredUser authrorize(Map<String, Object> params) throws Exception;
}
