package ua.cn.stu.rental.services;

import ua.cn.stu.rental.domain.Advertisement;

import java.util.List;
import java.util.Map;

public interface IAdvertisementService extends IRentarGenericService<Advertisement> {
    public List<Advertisement> getFilteredAds(Map<String, Object> params, Map<String,
            List<Object>> arrayParams, int pageIndex, int pageSize) throws Exception;

    public Advertisement createAd(Advertisement ad) throws Exception;
    public List<Advertisement> getAdsByUser(Map<String, Object> params,
                                            Map<String, List<Object>> arrayParams,
                                            int pageIndex, int pageSize) throws Exception;
}
