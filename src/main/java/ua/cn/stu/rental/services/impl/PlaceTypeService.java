package ua.cn.stu.rental.services.impl;

import org.springframework.stereotype.Service;
import ua.cn.stu.rental.domain.PlaceType;
import ua.cn.stu.rental.services.IPlaceTypeService;

@Service
public class PlaceTypeService extends GenericServiceImpl<PlaceType> implements IPlaceTypeService {
}
