package ua.cn.stu.rental.ws.endpoints;

import org.apache.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ua.cn.stu.rental.services.IAdvertisementService;
import ua.cn.stu.rental.ws.beans.CreateAdvertisementRequest;
import ua.cn.stu.rental.ws.beans.CreateAdvertisementResponse;
import ua.cn.stu.rental.ws.beans.GetFilteredAdsRequest;
import ua.cn.stu.rental.ws.beans.GetFilteredAdsResponse;
import ua.cn.stu.rental.ws.util.DTOtoDomainUtil;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Endpoint
public class AdvertisementEndPoint implements IAdvertisementWebService {
    public static final String TARGET_NAMESPACE = "http://stu.cn.ua/renstal-ws/schemas";
    private static org.apache.log4j.Logger LOG = LogManager.getLogger(AdvertisementEndPoint.class);
    @Autowired
    private IAdvertisementService advertisementService;

    public AdvertisementEndPoint() {

    }

    @PayloadRoot(localPart = "CreateAdvertisementRequest", namespace = TARGET_NAMESPACE)
    @ResponsePayload
    @Override
    public CreateAdvertisementResponse createAdvertisement(@RequestPayload CreateAdvertisementRequest req) {
        LOG.info("Creating advertisement web service started");
        ua.cn.stu.rental.domain.Advertisement adv = DTOtoDomainUtil.mapCreateAdvertisementRequest(req);
        ua.cn.stu.rental.domain.Advertisement res = null;
        try {
            res = advertisementService.createAd(adv);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            CreateAdvertisementResponse resp = new CreateAdvertisementResponse();
            resp.setId("001");
            return resp;
        }
        LOG.info("Creating advertisement web service ended");
        return DTOtoDomainUtil.mapCreateAdvertisementResponse(res);
    }

    @PayloadRoot(localPart = "GetFilteredAdsRequest", namespace = TARGET_NAMESPACE)
    @ResponsePayload
    @Override
    public GetFilteredAdsResponse getFilteredAds(@RequestPayload GetFilteredAdsRequest req) {
        Map<String, Object> params = DTOtoDomainUtil.mapFilterParams(req);
        LOG.info(params);
        List<ua.cn.stu.rental.domain.Advertisement> list = null;
        try {
            list = advertisementService.getFilteredAds(params, null, req.getPageIndex().intValue(), req.getPageSize().intValue());
        } catch (Exception e) {
            LOG.error(e.getMessage());
            list = new LinkedList<>();
        }
        LOG.info(list.size());
        GetFilteredAdsResponse response = new GetFilteredAdsResponse();
        for (ua.cn.stu.rental.domain.Advertisement domain: list) {
            response.getAds().add(DTOtoDomainUtil.mapAdvertisementDomainToDTO(domain));
        }

        return response;
    }

    public void setAdvertisementService(IAdvertisementService advertisementService) {
        this.advertisementService = advertisementService;
    }
}
