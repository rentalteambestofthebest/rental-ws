package ua.cn.stu.rental.ws.util;

import ua.cn.stu.rental.domain.Advertisement;
import ua.cn.stu.rental.domain.MeasurementType;
import ua.cn.stu.rental.domain.PlaceType;
import ua.cn.stu.rental.domain.RegisteredUser;
import ua.cn.stu.rental.ws.beans.*;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

public class DTOtoDomainUtil {

    public static RegisteredUser mapRegisterUserRequest(RegisterUserRequest req) {
        RegisteredUser registeredUser = new RegisteredUser();
        ua.cn.stu.rental.ws.beans.RegisteredUser dto = req.getUser();
        registeredUser.setEmail(dto.getEmail());
        registeredUser.setIsExtended(dto.isIsExtended());
        registeredUser.setLogin(dto.getLogin());
        registeredUser.setPassword(dto.getPassword());
        return registeredUser;
    }

    public static ua.cn.stu.rental.ws.beans.RegisterUserResponse mapRegisterUserResponse(RegisteredUser user) {
        RegisterUserResponse resp = new RegisterUserResponse();
        ua.cn.stu.rental.ws.beans.RegisteredUser dtoUser = new ua.cn.stu.rental.ws.beans.RegisteredUser();
        dtoUser.setEmail(user.getEmail());
        dtoUser.setIsExtended(user.isIsExtended());
        dtoUser.setLogin(user.getLogin());
        dtoUser.setPassword(user.getPassword());
        dtoUser.setId(user.getId());
        dtoUser.setRole(mapUserRoleDomainToDTO(user.getRole()));
        resp.setUser(dtoUser);
        return resp;
    }

    public static UserRole mapUserRoleDomainToDTO(ua.cn.stu.rental.domain.UserRole role) {
        UserRole dto = new UserRole();
        dto.setDescription(role.getDescription());
        dto.setName(role.getName());
        dto.setId(role.getId());
        return dto;
    }

    public static ua.cn.stu.rental.ws.beans.GetRegisteredUserResponse mapGetRegisteredUserResponse(RegisteredUser user) {
        GetRegisteredUserResponse resp = new GetRegisteredUserResponse();
        ua.cn.stu.rental.ws.beans.RegisteredUser dtoUser = new ua.cn.stu.rental.ws.beans.RegisteredUser();
        dtoUser.setEmail(user.getEmail());
        dtoUser.setIsExtended(user.isIsExtended());
        dtoUser.setLogin(user.getLogin());
        dtoUser.setPassword(user.getPassword());
        dtoUser.setId(user.getId());
        dtoUser.setRole(mapUserRoleDomainToDTO(user.getRole()));
        resp.setUser(dtoUser);
        return resp;
    }

    public static Advertisement mapCreateAdvertisementRequest(ua.cn.stu.rental.ws.beans.CreateAdvertisementRequest req) {
        Advertisement advertisement = new Advertisement();
        ua.cn.stu.rental.ws.beans.Advertisement ad = req.getAd();
        advertisement.setAddress(ad.getAddress());
        advertisement.setContactInformation(ad.getContactInfo());
        advertisement.setDescription(ad.getDescription());
        advertisement.setEntryDate(new Date());
        advertisement.setMeasurementType(mapMeasurementDTOtoDomain(ad.getMesType()));
        advertisement.setPlace(mapPlaceTypeDTOtoDomain(ad.getPlaceType()));
        advertisement.setPrice(ad.getPrice().intValue());
        advertisement.setSquare(ad.getSquare().intValue());
        advertisement.setTitle(ad.getTitle());
        advertisement.setUser(mapRegisteredUserDTOtoDomain(ad.getUser()));
        advertisement.setWeight(ad.getWeight().intValue());
        return advertisement;
    }

    public static ua.cn.stu.rental.ws.beans.CreateAdvertisementResponse mapCreateAdvertisementResponse(Advertisement advertisement) {
        CreateAdvertisementResponse response = new CreateAdvertisementResponse();
        response.setAd(mapAdvertisementDomainToDTO(advertisement));
        return response;
    }

    public static MeasurementType mapMeasurementDTOtoDomain(ua.cn.stu.rental.ws.beans.MeasurementType type) {
        MeasurementType measurementType = new MeasurementType();
        measurementType.setName(type.getName());
        measurementType.setId(type.getId());

        return measurementType;
    }

    public static PlaceType mapPlaceTypeDTOtoDomain(ua.cn.stu.rental.ws.beans.PlaceType type) {
        PlaceType placeType = new PlaceType();
        placeType.setName(type.getName());
        placeType.setId(type.getId());

        return placeType;
    }

    public static ua.cn.stu.rental.domain.UserRole mapUserRoleDTOtoDomain(ua.cn.stu.rental.ws.beans.UserRole role) {
        ua.cn.stu.rental.domain.UserRole userRole = new ua.cn.stu.rental.domain.UserRole();
        userRole.setDescription(role.getDescription());
        userRole.setName(role.getName());
        userRole.setId(role.getId());

        return userRole;
    }

    public static RegisteredUser mapRegisteredUserDTOtoDomain(ua.cn.stu.rental.ws.beans.RegisteredUser user) {
        RegisteredUser registeredUser = new RegisteredUser();
        registeredUser.setEmail(user.getEmail());
        registeredUser.setIsExtended(user.isIsExtended());
        registeredUser.setLogin(user.getLogin());
        registeredUser.setPassword(user.getPassword());
        registeredUser.setRole(mapUserRoleDTOtoDomain(user.getRole()));
        registeredUser.setId(user.getId());

        return registeredUser;
    }

    public static ua.cn.stu.rental.ws.beans.Advertisement mapAdvertisementDomainToDTO(Advertisement domain) {
        ua.cn.stu.rental.ws.beans.Advertisement dto = new ua.cn.stu.rental.ws.beans.Advertisement();
        dto.setAddress(domain.getAddress());
        dto.setContactInfo(domain.getAddress());
        dto.setDescription(domain.getDescription());

        GregorianCalendar c = new GregorianCalendar();
        c.setTime(domain.getEntryDate());
        XMLGregorianCalendar date2 = null;
        try {
            date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }

        dto.setEntryDate(date2);
        dto.setMesType(mapMeasurementTypeDomainToDTO(domain.getMeasurementType()));
        dto.setPlaceType(mapPlaceTypeDomainToDTO(domain.getPlace()));
        dto.setPrice(BigInteger.valueOf(domain.getPrice()));
        dto.setSquare(BigInteger.valueOf(domain.getSquare()));
        dto.setTitle(domain.getTitle());
        dto.setUser(mapRegisteredUserDomainToDTO(domain.getUser()));
        dto.setWeight(BigInteger.valueOf(domain.getWeight()));
        dto.setId(domain.getId());
        return dto;
    }

    public static ua.cn.stu.rental.ws.beans.MeasurementType mapMeasurementTypeDomainToDTO(MeasurementType domain) {
        ua.cn.stu.rental.ws.beans.MeasurementType dto = new ua.cn.stu.rental.ws.beans.MeasurementType();
        dto.setName(domain.getName());
        dto.setId(domain.getId());
        return dto;
    }

    public static ua.cn.stu.rental.ws.beans.PlaceType mapPlaceTypeDomainToDTO(PlaceType domain) {
        ua.cn.stu.rental.ws.beans.PlaceType dto = new ua.cn.stu.rental.ws.beans.PlaceType();
        dto.setName(domain.getName());
        dto.setId(domain.getId());
        return dto;
    }

    public static ua.cn.stu.rental.ws.beans.RegisteredUser mapRegisteredUserDomainToDTO(RegisteredUser domain) {
        ua.cn.stu.rental.ws.beans.RegisteredUser dto = new ua.cn.stu.rental.ws.beans.RegisteredUser();
        dto.setEmail(domain.getEmail());
        dto.setIsExtended(domain.isIsExtended());
        dto.setLogin(domain.getLogin());
        dto.setPassword(domain.getPassword());
        dto.setRole(mapUserRoleDomainToDTO(domain.getRole()));
        dto.setId(domain.getId());
        return dto;
    }

    public static Map<String, Object> mapFilterParams(GetFilteredAdsRequest req) {
        Map<String, Object> params = new HashMap<>();
        for (GetFilteredAdsRequest.Params.Entry entry: req.getParams().getEntry()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
            if (entry.getKey().equals("title")) {
                params.put(entry.getKey(), "%" + entry.getValue() + "%");
            }
            if (entry.getKey().equals("lowprice")) {
                params.put(entry.getKey(), Integer.parseInt(entry.getValue()));
            }
            if (entry.getKey().equals("highprice")) {
                params.put(entry.getKey(), Integer.parseInt(entry.getValue()));
            }
        }

        return params;
    }
}
