package ua.cn.stu.rental.ws.endpoints;


import ua.cn.stu.rental.ws.beans.CreateAdvertisementRequest;
import ua.cn.stu.rental.ws.beans.CreateAdvertisementResponse;
import ua.cn.stu.rental.ws.beans.GetFilteredAdsRequest;
import ua.cn.stu.rental.ws.beans.GetFilteredAdsResponse;

public interface IAdvertisementWebService {
    public CreateAdvertisementResponse createAdvertisement(CreateAdvertisementRequest req);
    public GetFilteredAdsResponse getFilteredAds(GetFilteredAdsRequest req);
}
