package ua.cn.stu.rental.ws.endpoints;

import ua.cn.stu.rental.ws.beans.GetRegisteredUserRequest;
import ua.cn.stu.rental.ws.beans.GetRegisteredUserResponse;
import ua.cn.stu.rental.ws.beans.RegisterUserRequest;
import ua.cn.stu.rental.ws.beans.RegisterUserResponse;

public interface IRegisterUserWebService {
    public GetRegisteredUserResponse getUser (GetRegisteredUserRequest registeredUserRequest);
    public RegisterUserResponse registerUser(RegisterUserRequest registeredUserRequest);
}
