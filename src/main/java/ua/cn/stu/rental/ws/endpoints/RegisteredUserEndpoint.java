package ua.cn.stu.rental.ws.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ua.cn.stu.rental.services.IRegisteredUserService;
import ua.cn.stu.rental.services.IUserRoleService;
import ua.cn.stu.rental.ws.beans.*;

import java.util.HashMap;
import java.util.Map;

import static ua.cn.stu.rental.ws.util.DTOtoDomainUtil.mapRegisterUserRequest;
import static ua.cn.stu.rental.ws.util.DTOtoDomainUtil.mapRegisterUserResponse;
import static ua.cn.stu.rental.ws.util.DTOtoDomainUtil.mapGetRegisteredUserResponse;

@Endpoint
public class RegisteredUserEndpoint implements IRegisterUserWebService {
    public static final String TARGET_NAMESPACE = "http://stu.cn.ua/renstal-ws/schemas";

    private ObjectFactory objectFactory;
    @Autowired
    private IRegisteredUserService userService;
    @Autowired
    private IUserRoleService roleService;

    public RegisteredUserEndpoint() {

    }

    @PayloadRoot(localPart = "RegisterUserRequest", namespace = TARGET_NAMESPACE)
    @ResponsePayload
    @Override
    public RegisterUserResponse registerUser(@RequestPayload RegisterUserRequest registeredUserRequest) {
        ua.cn.stu.rental.domain.RegisteredUser registeredUser = mapRegisterUserRequest(registeredUserRequest);
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("name", "user");
        ua.cn.stu.rental.domain.UserRole role = null;
        try {
            role = roleService.getUserRoleByName(param);
            registeredUser.setRole(role);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ua.cn.stu.rental.domain.RegisteredUser savedUser = null;
        try {
            savedUser = userService.register(registeredUser);
        } catch (Exception e) {
            e.printStackTrace();
        }

        RegisterUserResponse response = mapRegisterUserResponse(savedUser);
        response.setId("000");

        return  response;
    }

    @PayloadRoot(localPart = "GetRegisteredUserRequest", namespace = TARGET_NAMESPACE)
    @ResponsePayload
    @Override
    public GetRegisteredUserResponse getUser (@RequestPayload GetRegisteredUserRequest registeredUserRequest) {
        GetRegisteredUserResponse response;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pass", registeredUserRequest.getPassword());
        params.put("login", registeredUserRequest.getLogin());

        ua.cn.stu.rental.domain.RegisteredUser user = null;
        try {
             user = userService.authrorize(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        response = mapGetRegisteredUserResponse(user);
        return  response;
    }

    public ObjectFactory getObjectFactory() {
        if (this.objectFactory == null) {
            this.objectFactory = new ObjectFactory();
        }
        return objectFactory;
    }

    public void setObjectFactory(ObjectFactory objectFactory) {
        this.objectFactory = objectFactory;
    }
}
